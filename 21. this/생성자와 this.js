// new 키워드를 함수 호출 앞에 붙이면
// 이 함수 내부의 this는 새 객체(prototype)로 생성되고, 이 this를 자동적으로 반환한다.
// new를 붙이지 않으면 전역객체가 할당된다.
var funcThis = null;

function Func(){
  funcThis = this;
  return 1;
}

var o1 = Func();
if(funcThis === global){
  console.log('funcThis === global');
}

var o2 = new Func(); // Func는 1을 반환하게 되어있지만 new 키워드에 의해 this가 반환된다.
if(o2 !== 1 && funcThis === o2){
  console.log('o2 !== 1 && funcThis === o2');
}