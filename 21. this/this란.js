// this는 함수의 context를 의미한다.
// 객체에 속해있지 않은 함수에서 this는 전역객체(global, window)와 같다.

function func()
{
  if(this === global)
  {
    console.log('this === global');
  }
  else if(this === o)
  {
    console.log('this === o');
  }
  else {
    console.log('else');
  }
}

func();
console.log();

var o = {}
o.func = func;

func(); // this === global
func.call(o); // this === o
func.apply(o); // this === o
o.func(); // this === o