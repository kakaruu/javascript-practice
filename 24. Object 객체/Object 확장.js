// Object를 확장해서 모든 객체에서 사용 가능한 기능을 추가할 수 있다.

Object.prototype.contain = function(neddle) { // 해당 값이 있다면 true를 반환하는 메소드 추가
  for(var name in this){
      if(this[name] === neddle){
          return true;
      }
  }
  return false;
}
var o = {'name':'egoing', 'city':'seoul'}
console.log(o.contain('egoing'));
var a = ['egoing','leezche','grapittie'];
console.log(a.contain('leezche'));

// Object를 확장하면 생기는 문제점.
var o = {'name':'egoing', 'city':'seoul'}
for(var n in o) // 기대하는 결과 [name, city]
{
  console.log(n);
}
// 실제 결과 [name, city, contain]
// 즉 in을 사용하는 모든 곳에서 잠재적인 문제가 발생시킬 수 있다.


// for in에서의 문제 해결 방법
for(var n in o) // 기대하는 결과 [name, city]
{
  if(o.hasOwnProperty(n)) // 상속된거 제외한 프로퍼티를 체크해줌
  {
    console.log(n);
  }
}
// 실제 결과 [name, city]