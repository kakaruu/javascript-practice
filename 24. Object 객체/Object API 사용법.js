// Object의 API 문서에는
// Object 아래에 바로 정의되어 있는 Object.keys() 함수와
// Object.prototype에 정의되어 있는 Object.prototype.toString() 함수가 있다.
// Object.xxx와 Object.prototype.xxx의 차이를 알아보자.

// Object.keys()
// keys는 Object라는 메소드 객체에 정의된 메소드로, 정적(static) 메소드처럼 사용된다.
var arr = ['a', 'b', 'c'];
console.log('Object.keys(arr)', Object.keys(arr));

var x = {name: 'jhlee', age: 20, city: 'seoul'};
console.log('Object.keys(x)', Object.keys(x));

// Object.prototype.toString()
// prototype 아래에 정의된 toString은 생성된 인스턴스에서 사용된다.
var o = new Object();
console.log('o.toString()', o.toString());

var a = new Array(1, 2, 3);
console.log('a.toString()', a.toString());

// 궁금한점
// Object.prototype을 변경하면 Object도 변경될까?
// -> 된다
Object.prototype.testValue = '테스트';
console.log('o.testValue', o.testValue); // 테스트 정상 출력
console.log('Object.testValue', Object.testValue); // 테스트 정상 출력