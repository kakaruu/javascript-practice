var vscope = '전역';
function fscope() {
  var vscope = '지역';
  console.log(vscope); // 지역
}
fscope();
console.log(vscope); // 전역

function fscope2() {
  vscope2 = '전역?'; // var를 안쓰면 이놈은 전역변수로 생성됨
  console.log(vscope2); // 전역?
}
fscope2();
console.log(vscope2); // 전역?

{
  // javascript에서 지역변수는 함수 범위에서만 지정할 수 있다. 블록이 의미가 없음
  var vscope3 = '전역3'; // 블록 안에서 생성되었지만 전역변수임. for문, if문 안에서 생성된 변수도 다 전역으로 생성됨
}
console.log(vscope3); // 전역3

var i = 5;
function a() {
  var i = 10; // 이 범위에서 새로운 i를 생성하고 b를 실행해도 의미 없음
  b();
}
function b() {
  console.log(i); // 5. 무조건 전역변수인 i를 바라본다.
}
a();
b(); // 5

// => 즉, 참조하는 변수의 범위는 사용되는 시점이 아닌, 정의되는 시점의 범위를 따른다. (= 정적유효범위 =렉시컬)