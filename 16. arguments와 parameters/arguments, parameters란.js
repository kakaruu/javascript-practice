// arguments는 함수가 전달받은 인자값을 담고 있는 변수로,
// 배열과 유사한 형태(배열은 아님)로 함수의 인자 정보를 담고 있다.

// 매개변수(parameters)와 인자(arguments)의 차이
// sum(a, b)에서 a, b는 매개변수
// sum(1, 2)에서 1과 2는 인자
//
// 즉 매개변수는 함수가 받는 입력의 포맷을 의미
// 인자는 함수가 실행될 때 전달받는 값을 의미

function func(param1, param2) {
  var params_length = func.length; // 매개변수의 수. // 함수 외부에서도 접근 가능
  var args_length = arguments.length; // 인자의 수. // 함수 내부에서만 접근 가능

  console.log(params_length + ', ' + args_length);
}
func(1); // 결과: 2, 1

function sum(){ // sum에는 매개변수가 없음
  var i, _sum = 0;
  for(i = 0; i < arguments.length; i++){
    console.log(i+' : '+arguments[i]+'<br />');
      _sum += arguments[i];
  }
  return _sum;
}
console.log('result : ' + sum(1,2,3,4)); // javascript는 매개변수가 없는 함수에 인자를 넘길 수 있음. 너무 자유로운데.. 이래서 Typescript 쓰나보다.