// 두 방법 모두 동일한 결과
var pattern = /a/; // /사이에 정규식 패턴을 입력해서 생성할 수 있음
var pattern2 = new RegExp('a'); // RegExp 생성자로도 패턴을 생성할 수 있음

// 글로벌 패턴
// 글로벌 패턴으로 설정해야지만 Match, MatchAll, Replace 함수에서 일치하는 모든 부분이 발견됨
var pattern3 = /a/g;
pattern.global = true;

// 대소문자 구분 무시
var pattern4 = /a/i;
pattern.ignoreCase = true;