var pattern = /a./;
var pattern_global = /a./g;

var a = 'apart';
var b = 'node';
var c = 'apple';

console.log(pattern.exec(a)); // 맨 앞의 ap만 발견되고 상세 내용이 배열에 포함됨
console.log(pattern_global.exec(a)); // 맨 앞의 ap만 발견되고 상세 내용이 배열에 포함됨
console.log(pattern.exec(b)); // 찾지 못 하면 null이 반환됨

console.log(pattern.test(a)); // true
console.log(pattern.test(b)); // false

console.log(a.match(pattern)); // 맨 앞의 ap만 발견되고 상세 내용이 배열에 포함됨
console.log(a.match(pattern_global)); // ap와 ar이 배열로 반환됨
console.log(c.match(pattern_global)); // ap만 배열로 반환됨
var matches = [...a.matchAll(pattern)]; // 맨 앞의 ap만 발견되고 상세 내용이 배열에 포함됨
for(match of matches)
{
  console.log(match);
}
var matches = [...a.matchAll(pattern_global)]; // 매칭되는 모든 항목이 발견되고 상세 내용이 배열에 포함됨
for(match of matches)
{
  console.log(match);
}
console.log(a.replace(pattern, 'xyz')); // 맨 앞의 ap가 xyz로 바뀜
console.log(a.replace(pattern_global, 'xyz')); // 매칭되는 모든 항목이 xyz로 바뀜
