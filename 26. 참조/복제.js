// 복제
// 원시 타입은 다른 변수에 할당될 때 복제가 발생한다.
var a = 1;
var b = a; // 복제 발생
b = 2;
console.log(a); // 1이 출력됨. a는 참조타입(객체)이 아닌 원시 타입인 숫자이기 때문


// 참조
var a = {'id':1};
var b = a;
b.id = 2;
console.log(a.id); // 2가 출력됨

// Call by Value
var a = 1;
function func(b)
{
  b = 2;
}
func(a);
console.log(a); // 1

// Call by Reference
var a = {'id':1};
function func2(b)
{
  b.id = 2;
}
func2(a);
console.log(a.id); // 2