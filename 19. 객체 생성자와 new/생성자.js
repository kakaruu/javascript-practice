function Person(name){
  // new 를 앞에 붙여서 함수를 호출하면 this에 prototype의 설정이 들어간다.
  // new 없이 함수만 호출하면 this에 global 객체가 들어간다.
  // 특이한 점은 인스턴스를 생성하고 나서 prototype을 변경해도 앞에서 생성된 인스턴스에도 영향을 준다는 점
  this.name = name;
  this.introduce = function(){
      return 'My name is '+this.name;
  }
}
var p1 = new Person('egoing');
console.log(p1.introduce()+"<br />");

var p2 = new Person('leezche');
console.log(p2.introduce());

var p3 = Person('jerry'); // new 없이 호출된 경우 global 객체에 name이라는 멤버가 추가된다.
var p3 = Person('terry'); // global 객체의 기존의 name이 jerry에서 terry로 변경된다.
console.log(p3.introduce()); // 오류