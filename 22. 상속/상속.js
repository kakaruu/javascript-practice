// https://shlee1353.github.io/2019/07/05/js-prototype-inheritance/
// 위 링크에서 잘 설명하고 있음

function Person(name){
  this.name = name;
}
Person.prototype.name=null;
Person.prototype.introduce = function(){
  return 'My name is '+this.name;
}

function Programmer(name){
  this.name = name;
}
Programmer.prototype = new Person(); // Person 상속

// Programmer.prototype = new Person();로 상속을 하게 되면
// instanceof로 p1을 확인했을 때 Person과 Programmer의 인스턴스로 정상적으로 확인이 되지만
// p1의 생성자를 확인하면 Programmer가 아닌 Person으로 되어있는 것을 볼 수 있다.
Programmer.prototype.constructor = Programmer; // constructor를 Person으로 설정해준다.
// Programmer.prototype = Person.prototype; // 이러면 안 됨. 자식의 prototype이 변경되면 부모의 prototype도 변경된다. // deep clone을 하면 괜찮을 듯 하다.
Programmer.prototype.coding = function() {
  return 'hello world';
}

var p1 = new Programmer('egoing');
console.log(p1.introduce());
console.log(p1.coding());

// Constructor를 제대로 연결하기 위해서 Object.create()가 표준으로 등장했다.
// Object.create = function(o) {
//   function F() {}
//   F.prototype = o;
//   return new F();
//  }
// Object.create()로 상속하는 예시 IE9부터 사용 가능
Programmer.prototype = Object.create(Person.prototype);
Programmer.prototype.constructor = Programmer;

// ES6부터는 class를 사용해서 구현할수도 있다.
// 예시
class Vehicle {
  constructor (name, type) {
    this.name = name;
    this.type = type;
  }
  getName () {
    return this.name;
  }
  getType () {
    return this.type;
  }
}
class Car extends Vehicle {
  constructor (name) {
    super(name, 'car');
  }
  getName () {
    return 'It is a car: ' + super.getName();
  }
}
let car = new Car('Tesla');
console.log(car.getName()); // It is a car: Tesla
console.log(car.getType()); // car