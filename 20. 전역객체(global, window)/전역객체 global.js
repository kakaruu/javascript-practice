function func() {
  console.log(this);
}
func();
var name2 = 'test';
console.log(name2);
console.log(global.name2);
// 웹 브라우저에서는 window.func();
// nodejs에서는 global.func(); 라고 하는데 안된다?