function func() {
}
func();

function sum(p1, p2)
{
  return p1 + p2;
}
console.log(sum(1, 2)); // 인자를 배열로 입력하려면 귀찮음
console.log(sum.apply(null, [1, 2])); // 인자를 배열로 입력할 수 있긴한데, 맨 처음 인자를 null로 쓰는 것은 바람직하지 않음
console.log(sum.call(null, 1, 2)); // 마찬가지로 맨 처음 인자를 null로 쓰는 것은 바람직하지 않음

// applay와 call의 맨 앞의 인자값은 thisArg로,
// 메소드 내부에서 this를 참조하는 경우,
// 이 this를 thisArg에 입력한 객체로 치환해주는 인자값이다.
// thisArg를 null로 입력할 경우 함수 내부에서 this는 global이라는 객체를 참조하게 된다.

// 예를들어 이렇게 사용할 수 있다.
let person1 = {
  name: 'Jo'
};
let person2 = {
  name: 'Kim',
  study: function() {
      console.log(this.name + '이/가 공부를 하고 있습니다.');
  }
};
person2.study(); // Kim이/가 공부를 하고 있습니다.
person2.study.call(person1); // Jo이/가 공부를 하고 있습니다.